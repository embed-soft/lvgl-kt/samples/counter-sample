package org.example.counter

import io.gitlab.embedSoft.lvglKt.core.initLvgl
import io.gitlab.embedSoft.lvglKt.drivers.input.initEvdev
import io.gitlab.embedSoft.lvglKt.frameBuffer.FrameBuffer
import kotlin.system.exitProcess

internal actual fun exitProgram(exitCode: Int) {
    exitProcess(exitCode)
}

internal fun initSubSystems() {
    initLvgl()
    FrameBuffer.open()
    initEvdev()
}
