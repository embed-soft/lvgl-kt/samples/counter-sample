package org.example.counter

import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.LVGL_VERSION_MAJOR
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.LVGL_VERSION_MINOR
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.LVGL_VERSION_PATCH
import io.gitlab.embedSoft.lvglKt.core.runEventLoop
import io.gitlab.embedSoft.lvglKt.drivers.input.setupTouchScreen
import io.gitlab.embedSoft.lvglKt.frameBuffer.FrameBuffer

fun main(args: Array<String>) {
    println("Starting Counter sample...")
    val resolution = processProgramArgs(args)
    initSubSystems()
    FrameBuffer.createDisplay(newHorRes = resolution.first.toShort(), newVertRes = resolution.second.toShort(),
        enableDblBuffer = false)
    setupTouchScreen()
    setupUi()
    println("LVGL Version: $LVGL_VERSION_MAJOR.$LVGL_VERSION_MINOR.$LVGL_VERSION_PATCH")
    runEventLoop()
}
