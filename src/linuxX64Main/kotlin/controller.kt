package org.example.counter

import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.LV_FONT_DEFAULT
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.LV_PALETTE_RED
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.LV_THEME_DEFAULT_DARK
import io.gitlab.embedSoft.lvglKt.core.display.DisplayDrawBuffer
import io.gitlab.embedSoft.lvglKt.core.display.DisplayDriver
import io.gitlab.embedSoft.lvglKt.core.initLvgl
import io.gitlab.embedSoft.lvglKt.core.styling.colorFromRgb
import io.gitlab.embedSoft.lvglKt.core.styling.mainPaletteColor
import io.gitlab.embedSoft.lvglKt.core.styling.toFont
import io.gitlab.embedSoft.lvglKt.drivers.Display
import io.gitlab.embedSoft.lvglKt.drivers.Theme
import io.gitlab.embedSoft.lvglKt.sdl2.Sdl2
import kotlin.system.exitProcess

private var displayBuffer: DisplayDrawBuffer? = null
private var displayDriver: DisplayDriver? = null
private var display: Display? = null

internal actual fun exitProgram(exitCode: Int) {
    exitProcess(exitCode)
}

internal fun initSubSystems() {
    initLvgl()
    val tmp = Sdl2.initHal(true)
    displayBuffer = tmp.first
    displayDriver = tmp.second
    display = tmp.third
    display?.theme = Theme.create(
        display = display!!,
        primaryColor = colorFromRgb(red = 0u, green = 180u, blue = 0u),
        secondaryColor = mainPaletteColor(LV_PALETTE_RED),
        dark = LV_THEME_DEFAULT_DARK == 1,
        fonts = arrayOf(LV_FONT_DEFAULT.toFont())
    )
}
