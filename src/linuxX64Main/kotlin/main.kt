package org.example.counter

import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.LVGL_VERSION_MAJOR
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.LVGL_VERSION_MINOR
import io.gitlab.embedSoft.lvglKt.core.clib.lvgl.LVGL_VERSION_PATCH
import io.gitlab.embedSoft.lvglKt.core.runEventLoop

fun main() {
    println("Starting Counter sample...")
    initSubSystems()
    setupUi()
    println("LVGL Version: ${LVGL_VERSION_MAJOR}.${LVGL_VERSION_MINOR}.${LVGL_VERSION_PATCH}")
    runEventLoop()
}
