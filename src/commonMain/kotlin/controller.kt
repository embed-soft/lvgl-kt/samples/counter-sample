package org.example.counter

internal fun processProgramArgs(args: Array<String>): Pair<UInt, UInt> {
    if (args.isEmpty()) {
        println("""
            The following program arguments are supported:
              --horRes : Horizontal resolution
              --vertRes : Vertical resolution
            
            Example: counter --horRes=800 --vertRes=600
        """.trimIndent())
        exitProgram(0)
    }
    return extractHorRes(args) to extractVertRes(args)
}

private fun extractVertRes(args: Array<String>) = try {
    (args.find { it.startsWith("--vertRes=") } ?: "")
        .replace("--vertRes=", "")
        .toUInt()
} catch (ex: NumberFormatException) {
    0u
}

private fun extractHorRes(args: Array<String>) = try {
    (args.find { it.startsWith("--horRes=") } ?: "")
        .replace("--horRes=", "")
        .toUInt()
} catch (ex: NumberFormatException) {
    0u
}

internal expect fun exitProgram(exitCode: Int)
