package org.example.counter

import io.gitlab.embedSoft.lvglKt.core.Screen
import io.gitlab.embedSoft.lvglKt.core.event.LvglEvent
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventCallback
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventType
import io.gitlab.embedSoft.lvglKt.core.event.value
import io.gitlab.embedSoft.lvglKt.core.lvglObject.lvglObject
import io.gitlab.embedSoft.lvglKt.core.styling.blackColor
import io.gitlab.embedSoft.lvglKt.core.styling.setTextColorStyle
import io.gitlab.embedSoft.lvglKt.coreWidgets.input.button
import io.gitlab.embedSoft.lvglKt.coreWidgets.output.Label
import io.gitlab.embedSoft.lvglKt.coreWidgets.output.label

private var counter = 1u

private fun onButtonClicked(@Suppress("UNUSED_PARAMETER") evt: LvglEvent, userData: Any?) {
    counter++
    (userData as Label).text = "Counter $counter"
}

internal fun setupUi() {
    val screen = lvglObject(null) {}
    val btn = button(screen) { center() }
    val lbl = label(btn) {
        text = "Counter $counter"
        setTextColorStyle(blackColor())
    }
    val callback = LvglEventCallback.create(lbl, ::onButtonClicked)
    btn.addEventCallback(LvglEventType.CLICKED.value, callback)
    Screen.load(screen)
}
