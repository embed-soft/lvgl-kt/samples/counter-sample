group = "org.embedsoft"
version = "0.1"

plugins {
    kotlin("multiplatform") version "1.7.21"
}

repositories {
    mavenCentral()
    mavenLocal()
}

kotlin {
    val lvglKtVer = "0.4.0"
    val programEntryPoint = "org.example.counter.main"

    linuxArm32Hfp("linuxArm32") {
        compilations.getByName("main") {
            dependencies {
                implementation("io.gitlab.embed-soft:lvglkt-core:$lvglKtVer")
                implementation("io.gitlab.embed-soft:lvglkt-drivers:$lvglKtVer")
                implementation("io.gitlab.embed-soft:lvglkt-frame-buffer:$lvglKtVer")
                implementation("io.gitlab.embed-soft:lvglkt-core-widgets:$lvglKtVer")
            }
        }
        binaries {
            executable("counter") {
                linkerOpts(
                    "--library-path=lib/linuxArm32",
                    "--library=lvgl",
                    "--library=lv_drivers"
                )
                entryPoint = programEntryPoint
            }
        }
    }

    linuxX64 {
        compilations.getByName("main") {
            dependencies {
                implementation("io.gitlab.embed-soft:lvglkt-core:$lvglKtVer")
                implementation("io.gitlab.embed-soft:lvglkt-drivers:$lvglKtVer")
                implementation("io.gitlab.embed-soft:lvglkt-sdl2:$lvglKtVer")
                implementation("io.gitlab.embed-soft:lvglkt-core-widgets:$lvglKtVer")
            }
        }
        binaries {
            executable("counter") {
                linkerOpts(
                    "-L/usr/lib/x86_64-linux-gnu",
                    "-lSDL2",
                    "--library-path=lib/linuxX64",
                    "--library=lvgl",
                    "--library=lv_drivers"
                )
                entryPoint = programEntryPoint
            }
        }
    }

    sourceSets {
        commonMain {
            dependencies {
                val kotlinVer = "1.7.21"
                implementation(kotlin("stdlib", kotlinVer))
                implementation("io.gitlab.embed-soft:lvglkt-core:$lvglKtVer")
                implementation("io.gitlab.embed-soft:lvglkt-drivers:$lvglKtVer")
                implementation("io.gitlab.embed-soft:lvglkt-core-widgets:$lvglKtVer")
            }
        }
    }
}
